# NTO_Robotics

# Linux
---
Инстуркция по началу работы
1. Склонируйте ребпозиторий `git clone https://gitlab.com/ovcharov.alex.o/nto_robotics.git -b nto-2023-ii`
2. Участникам нужно установить зависимости (смотри ниже `Установка зависимостей на Linux (Ubuntu 20.04)`)
3. Установите `.deb` и пакет `PyPi` (python) (смотри ниже `Установка симулятора`)
4. Разработчикам на языке `python` нужно запустить пример решения первой задачи
```bash
$ cd nto_robotics/python
$ python3 solution1.py 
```
5. Для программистов `C++` нужно собрать проект:
```bash
$ cd nto_robotics/cpp
$ mkdir build && cd build
$ cmake ..
$ make -j4
```
и запустить решение `./solution1`

### Установка зависимостей на Linux (Ubuntu 20.04):
```bash
$ sudo apt-get update && sudo apt-get upgrade
$ sudo apt install vim build-essential cmake g++ libopencv-dev software-properties-common \
      mesa-utils libcanberra-gtk-module libcanberra-gtk3-module -y

$ sudo apt install python3 python3-distutils python3-dev python3-pip -y
$ sudo python3 -m pip install opencv-python
```

### Установка/Переустановка симулятора
1. Установка `.deb` пакета (**Обязательно!!!**)
```bash
$ cd nto_robotics/simulator
$ sudo apt install ./participant_nto_simlib_1.0-1_amd64.deb -y
```

1. Установка `PyPi` пакета (Поддерживаются только версии **python3.6**, **python3.8**, **python3.10**)
```bash
$ cd nto_robotics/simulator
$ python3 -m pip install ./nto_sim-0.0.1.tar.gz
```

2.  Путь линковщику
```bash
$ echo "ldconfig /usr/local/lib/nto_sim/ >> ~/.bashrc"
```

3. Чтобы переустановить симулятор вам нужно удалить старый `deb`
```bash
$ sudo apt remove nto-simlib
```

4. Повторить шаги 1 по установке `deb` и `PyPi` пакетов

### Изменения

1. Участники теперь должны готовить функцию `solve(task)` с аргументом типа `Task`. Пример
```python
def solve(task: Task) -> None
    sceneImg = task.getTaskScene()
    mapImage = task.getTaskMap()
    ...
```

2. При отправке
```python
response = task.sendMessage('p_1 OK')
```
система проверяет сообщение на правильность заполненности. Как результат, в ответе будет сообщение с ошибкой, если что-то не так.